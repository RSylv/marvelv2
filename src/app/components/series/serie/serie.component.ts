import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { HttpsPathService } from 'src/app/services/https-path.service';

@Component({
  selector: 'app-serie',
  templateUrl: './serie.component.html',
  styleUrls: ['./serie.component.scss']
})
export class SerieComponent implements OnInit {

  private id: number = this.route.snapshot.params.id;
  isWaiting: boolean = true;
  error: string = '';
  serie: any = null;
  listTitle: string[] = ['characters', 'comics', 'creators', 'stories', 'events'];

  constructor(private api: ApiService, private route: ActivatedRoute, private https: HttpsPathService) { }

  ngOnInit(): void {
    this.api.getElementById(this.id, 'series').subscribe(data => {
      this.serie = data.data.results[0]
      this.serie.thumbnail.path = this.https.httpsUrl(this.serie.thumbnail.path)
      this.isWaiting = false
    })
  }

  checkDate(date: string) {
    if (date.charAt(0) !== '-') {
      return date;
    }
    return '';
  }

}
