import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss']
})
export class StoriesComponent implements OnInit {

  private url: string = '';
  private order: string = '';
  isWaiting: boolean = true;
  name: string = 'stories';
  allStories: any[] = [];
  total: number = 0;
  offset: number = 0;
  limit: number = 0;
  page: number = 1;

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.api.getAllElements('stories').subscribe(data => {
      this.initData(data)
    })
  }

  initData(data: any) {
    this.allStories = data.data.results
    this.total = data.data.total;
    this.offset = data.data.offset;
    this.limit = data.data.limit;
    this.isWaiting = false;
  }

  viewDetail(id: number) {
    this.router.navigate(['stories', id])
  }

  pageUpOrDown(offset: number) {
    this.isWaiting = true;
    this.api.pageUpOrDown(offset, 'stories', this.url + this.order).subscribe(data => {
      this.allStories = data.data.results;
      this.isWaiting = false;
    })
  }

  onSearch(values: any) {

    this.order = values.order !== '' ? `&orderBy=${values.order}` : '';
    this.api.setLimit(values.limit)

    this.api.getElementsBySearchField(this.url + this.order, 'stories').subscribe(data => {
      this.initData(data)
    })
  }

  ngOnDestroy(): void {
    this.allStories = [];
  }


}
