export interface OrderSelect {
    text: string;
    value: string;
    order: string;
}