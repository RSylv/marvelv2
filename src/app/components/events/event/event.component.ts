import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { HttpsPathService } from 'src/app/services/https-path.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  private id: number = this.route.snapshot.params.id;
  isWaiting: boolean = true;
  error: string = '';
  event: any = null;
  listTitle: string[] = ['characters', 'comics', 'creators', 'series', 'stories'];

  constructor(private api: ApiService, private route: ActivatedRoute, private https: HttpsPathService) { }

  ngOnInit(): void {
    this.getEventByID(this.id)
  }

  getEventByID(id: number) {
    this.api.getElementById(this.id, 'events').subscribe(data => {
      this.event = data.data.results[0]
      this.event.thumbnail.path = this.https.httpsUrl(this.event.thumbnail.path)
      this.isWaiting = false
    })
  }

  goToEvent(url: string) {
    this.event = null;
    this.id = Number(url.slice(url.lastIndexOf('/') + 1, url.length))
    this.getEventByID(this.id)
  }

  checkDate(date: string) {
    if (date.charAt(0) !== '-') {
      return date;
    }
    return '';
  }

}
