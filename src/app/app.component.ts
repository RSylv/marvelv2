import { DOCUMENT } from '@angular/common';
import { Component, HostListener, Inject } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'marvelApi';
  isHide: boolean = false;
  windowScrolled: boolean = false;

  constructor(@Inject(DOCUMENT) private document: Document) { }
  
  toggleMenu() {
    this.isHide = !this.isHide;
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {    
    window.pageYOffset > 500 ?
      this.windowScrolled = true :
      this.windowScrolled = false;
  }
  
  scrollToTop() {
      (function smoothscroll() {
          var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
          if (currentScroll > 0) {
              window.requestAnimationFrame(smoothscroll);
              window.scrollTo(0, currentScroll - (currentScroll / 8));
          }
      })();
  }
}
