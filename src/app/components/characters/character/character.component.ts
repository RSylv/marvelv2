import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { HttpsPathService } from 'src/app/services/https-path.service';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {

  private id: number = this.route.snapshot.params.id;
  isWaiting: boolean = true;
  error: string = '';
  character: any = null;
  listTitle: string[] = ['comics', 'series', 'stories', 'events'];

  constructor(private api: ApiService, private route: ActivatedRoute, private https: HttpsPathService) {
  }

  ngOnInit(): void {
    this.api.getElementById(this.id, 'characters').subscribe(data => {
      this.character = data.data.results[0]
      this.character.thumbnail.path = this.https.httpsUrl(this.character.thumbnail.path)
      this.isWaiting = false
    })
  }
  
  checkDate(date: string) {
    if (date.charAt(0) !== '-') {
      return date;
    }
    return '';
  }

}
