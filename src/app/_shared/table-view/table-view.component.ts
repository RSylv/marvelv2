import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TableModel } from 'src/app/models/table.model';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.scss']
})
export class TableViewComponent implements OnInit {

  @Input() elements: TableModel = new TableModel();
  @Input() title: string = '';  
  @Input() elementName: string = '';  
  @Input() id: number = 0;  

  constructor(private router: Router, private api: ApiService) { }

  ngOnInit(): void {
    // console.log(this.elementName, this.elements, this.title);    
  }

  getIdOfUrl(url: string) {
    return Number(url.slice(url.lastIndexOf('/') + 1 , url.length))
  }

  link(id: number) {
    this.router.navigate([this.title, id])
  }

  fullCollection(url: string) {
    let endUrl = url.slice(36, url.lastIndexOf('/'))
    const name = endUrl.substring(0, endUrl.indexOf('/'))
    const id = endUrl.substring(endUrl.indexOf('/') + 1 , endUrl.length)
    this.router.navigate(['collection', name, id, this.title ], { queryParams: { elementName: this.elementName } })
  }

}
