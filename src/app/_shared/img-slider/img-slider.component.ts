import { Component, Input, OnInit } from '@angular/core';
import { ImageSliderModel } from 'src/app/interfaces/image.slider.interface';
import { HttpsPathService } from 'src/app/services/https-path.service';

@Component({
  selector: 'app-img-slider',
  templateUrl: './img-slider.component.html',
  styleUrls: ['./img-slider.component.scss']
})
export class ImgSliderComponent implements OnInit {

  @Input() images: any[] = [];
  
  imagesUrls: ImageSliderModel[] = []

  constructor(private https: HttpsPathService) { }

  ngOnInit(): void {
    this.images.forEach(img => {
      const safeUrl = this.https.httpsUrl(img.path) + '.' + img.extension
      const image = {
        image: safeUrl,
        thumbImage: safeUrl,
      }
      this.imagesUrls.push(image)
    })    
  }

}
