import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitAnimationComponent } from './wait-animation.component';

describe('WaitAnimationComponent', () => {
  let component: WaitAnimationComponent;
  let fixture: ComponentFixture<WaitAnimationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WaitAnimationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitAnimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
