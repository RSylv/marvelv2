import { enableProdMode, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { NgImageSliderModule } from 'ng-image-slider';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CharactersComponent } from './components/characters/characters.component';
import { CharacterComponent } from './components/characters/character/character.component';
import { ComicsComponent } from './components/comics/comics.component';
import { ComicComponent } from './components/comics/comic/comic.component';
import { SeriesComponent } from './components/series/series.component';
import { SerieComponent } from './components/series/serie/serie.component';
import { CreatorsComponent } from './components/creators/creators.component';
import { CreatorComponent } from './components/creators/creator/creator.component';
import { StoriesComponent } from './components/stories/stories.component';
import { StorieComponent } from './components/stories/storie/storie.component';
import { EventsComponent } from './components/events/events.component';
import { EventComponent } from './components/events/event/event.component';
import { PaginationComponent } from './_shared/pagination/pagination.component';
import { SearchFormComponent } from './_shared/search-form/search-form.component';
import { ImgSliderComponent } from './_shared/img-slider/img-slider.component';
import { ListViewComponent } from './_shared/list-view/list-view.component';
import { TableViewComponent } from './_shared/table-view/table-view.component';
import { FooterLinksComponent } from './_shared/footer-links/footer-links.component';
import { WaitAnimationComponent } from './_shared/wait-animation/wait-animation.component';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { CollectionsComponent } from './components/collections/collections.component';

enableProdMode();

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CharactersComponent,
    CharacterComponent,
    ComicsComponent,
    ComicComponent,
    SeriesComponent,
    SerieComponent,
    CreatorsComponent,
    CreatorComponent,
    StoriesComponent,
    StorieComponent,
    EventsComponent,
    EventComponent,
    PaginationComponent,
    SearchFormComponent,
    ImgSliderComponent,
    ListViewComponent,
    TableViewComponent,
    FooterLinksComponent,
    WaitAnimationComponent,
    CollectionsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgImageSliderModule,
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
