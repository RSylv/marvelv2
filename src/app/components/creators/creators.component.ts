import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CreatorModel } from 'src/app/interfaces/creator.interface';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-creators',
  templateUrl: './creators.component.html',
  styleUrls: ['./creators.component.scss']
})
export class CreatorsComponent implements OnInit {

  private url: string = '';
  private order: string = '';
  isWaiting: boolean = true;
  name: string = 'creators';
  allCreators: CreatorModel[] = [];
  total: number = 0;
  offset: number = 0;
  limit: number = 0;
  page: number = 1;

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.api.getAllElements('creators').subscribe(data => {
      this.initData(data)
    })
  }

  initData(data: any) {
    this.allCreators = data.data.results
    this.total = data.data.total;
    this.offset = data.data.offset;
    this.limit = data.data.limit;
    this.isWaiting = false;
  }

  viewDetail(id: number) {
    this.router.navigate(['creators', id])
  }

  pageUpOrDown(offset: number) {
    this.isWaiting = true;
    this.api.pageUpOrDown(offset, 'creators', this.url + this.order).subscribe(data => {
      this.allCreators = data.data.results;
      this.isWaiting = false;
    })
  }

  onSearch(values: any) {

    this.isWaiting = true;
    let x = values.firstname !== '' ? `firstNameStartsWith=${values.firstname}&` : '';
    let y = values.lastname !== '' ? `lastNameStartsWith=${values.lastname}` : '';
    this.url = x + y;
    this.order = values.order !== '' ? `&orderBy=${values.order}` : '';

    this.api.setLimit(values.limit)

    this.api.getElementsBySearchField(this.url + this.order, 'creators')
      .subscribe(data => { this.initData(data) })
    
  }

  ngOnDestroy(): void {
    this.allCreators = [];
  }

}
