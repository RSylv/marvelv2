import { Component, Input, OnInit } from '@angular/core';
import { LinkModel } from 'src/app/interfaces/link.interface';

@Component({
  selector: 'app-footer-links',
  templateUrl: './footer-links.component.html',
  styleUrls: ['./footer-links.component.scss']
})
export class FooterLinksComponent implements OnInit {

  @Input() urls: LinkModel[] = [];

  constructor() { }

  ngOnInit(): void {
    // console.log(this.urls);    
  }

}
