export interface LinkModel {
    type: string;
    url: string;
}