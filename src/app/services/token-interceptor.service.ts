import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Md5 } from 'ts-md5';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  private timestamp: string = new Date().getTime().toString()
  private hash: string = Md5.hashStr(this.timestamp + environment.privateKey + environment.publicKey).toString()
  private TOKEN: string = `&ts=${this.timestamp}&apikey=${environment.publicKey}&hash=${this.hash}`

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // console.log('INTERCEPTOR', req.url);

    let authUrl = req.url + this.TOKEN;

    const authReq = req.clone({ url: authUrl });

    return next.handle(authReq);
  }
}