import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ComicModel } from 'src/app/interfaces/comic.interface';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-comics',
  templateUrl: './comics.component.html',
  styleUrls: ['./comics.component.scss']
})
export class ComicsComponent implements OnInit {

  private url: string = ''
  private order: string = ''
  isWaiting: boolean = true;
  name: string = 'comics';
  allComics: ComicModel[] = [];
  total: number = 0;
  offset: number = 0;
  limit: number = 0;
  page: number = 1;

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.api.getAllElements('comics').subscribe(data => {
      this.initData(data)
    })
  }

  initData(data: any) {
    this.allComics = data.data.results
    this.total = data.data.total;
    this.offset = data.data.offset;
    this.limit = data.data.limit;
    this.isWaiting = false;
  }

  viewDetail(id: number) {
    this.router.navigate(['comics', id])
  }

  pageUpOrDown(offset: number) {
    this.isWaiting = true;
    this.api.pageUpOrDown(offset, 'comics', this.url + this.order).subscribe(data => {
      this.allComics = data.data.results;
      this.isWaiting = false;
    })
  }

  onSearch(values: any) {

    this.isWaiting = true;
    let x = values.search !== '' ? `titleStartsWith=${values.search}&` : '';
    let y = values.format !== '' ? `format=${values.format}` : '';
    this.url = x + y;
    this.order = values.order !== '' ? `&orderBy=${values.order}` : '';
    
    this.api.setLimit(values.limit)

    this.api.getElementsBySearchField(this.url + this.order, 'comics')
      .subscribe(data => { this.initData(data) })
    
  }

  ngOnDestroy(): void {
    this.allComics = [];
  }

}
