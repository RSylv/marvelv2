import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.scss']
})
export class CollectionsComponent implements OnInit {
  
  elementName: string = this.route.snapshot.queryParams.elementName
  name: string = this.route.snapshot.params.name;
  id: string = this.route.snapshot.params.id;
  title: string = this.route.snapshot.params.title;

  url: string = `${this.name}/${this.id}/${this.title}`;

  element: any = null;
  isWaiting: boolean = true;
  page: number = 1;

  constructor(private api: ApiService, private route: ActivatedRoute, private router: Router) { }

  // http://gateway.marvel.com/v1/public/events/269/characters 

  ngOnInit(): void {    
    this.api.getcollection(this.url).subscribe(data => {
      this.initData(data);     
    })
  }

  initData(data: any): void {
    this.element = data.data;
    this.isWaiting = false;
  }

  pageUpOrDown(offset: number) {
    this.isWaiting = true;
    this.api.pageUpOrDown(offset, this.url).subscribe(data => {
      this.initData(data);
    })
  }

  onSearch(values: any) {
    this.isWaiting = true;
    this.api.setLimit(values.limit)
    this.api.getElementsBySearchField('', this.url).subscribe(data => {
      this.initData(data)
    })
  }

  link(id: number) {
    this.router.navigate([this.title, id])
  }

}
