export class TableModel {
    available: number = 0
    collectionURI: string = ''
    items: any[] = [
        {
            resourceURI: '',
            name: '',
            type: ''
        }
    ]
    returned: number = 0
}
