import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.scss']
})
export class SeriesComponent implements OnInit {

  private url: string = '';
  private order: string = '';
  isWaiting: boolean = true;
  name: string = 'series';
  allSeries: any[] = [];
  total: number = 0;
  offset: number = 0;
  limit: number = 0;
  page: number = 1;

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.api.getAllElements('series').subscribe(data => {
      this.initData(data)
    })
  }

  initData(data: any): void {
    this.allSeries = data.data.results;
    this.total = data.data.total;
    this.offset = data.data.offset;
    this.limit = data.data.limit;
    this.isWaiting = false;
  }

  viewDetail(id: number) {
    this.router.navigate(['series', id])
  }

  pageUpOrDown(offset: number) {
    this.isWaiting = true;
    this.api.pageUpOrDown(offset, 'series', this.url + this.order).subscribe(data => {
      this.allSeries = data.data.results;
      this.isWaiting = false;
    })
  }

  onSearch(values: any) {    
    this.isWaiting = true;
    let x = values.search !== '' ? `titleStartsWith=${values.search}&` : '';
    let y = values.type !== '' ? `seriesType=${values.type}` : '';
    this.url = x + y;
    this.order = values.order !== '' ? `&orderBy=${values.order}` : '';

    this.api.setLimit(values.limit)

    this.api.getElementsBySearchField(this.url + this.order, 'series')
      .subscribe(data => { this.initData(data) })    
  }

  ngOnDestroy(): void {
    this.allSeries = [];
  }

}
