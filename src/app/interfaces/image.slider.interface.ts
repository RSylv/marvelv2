export interface ImageSliderModel {
    image: string,
    thumbImage: string,
    alt?: string,
    title?: string
}