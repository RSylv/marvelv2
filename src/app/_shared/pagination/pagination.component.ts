import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Output() changePageClick = new EventEmitter();
  @Input() total: number = 0;
  @Input() offset: number = 0;
  @Input() limit: number = 0;
  @Input() page: number = 1;

  constructor() { }

  ngOnInit(): void {
  }

  pageUpOrDown(offset: number) {
    this.offset += offset;

    if (this.offset < 0) {
      this.offset = 0;
      this.page = 1
      return;
    }
    else if (this.offset >= this.total) {
      this.page = this.page
      this.offset = this.total - this.limit;
    }
    else {
      this.page += offset.toString()[0] !== '-' ? 1 : -1;
    }

    this.changePageClick.emit(this.offset)
  }

  getPage(total: number, limit: number): number {
    const maxPage = (total / limit) > Math.floor(total / limit) ?
      Math.floor(total / limit) + 1 :
      Math.floor(total / limit);
    return maxPage;
  }

}
