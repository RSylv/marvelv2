import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  private url: string = '';
  private order: string = '';
  isWaiting: boolean = true;
  name: string = 'events';
  allEvents: any[] = [];
  total: number = 0;
  offset: number = 0;
  limit: number = 0;
  page: number = 1;

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.api.getAllElements('events').subscribe(data => {
      this.initData(data)
    })
  }

  initData(data: any): void {
    this.allEvents = data.data.results;
    this.total = data.data.total;
    this.offset = data.data.offset;
    this.limit = data.data.limit;
    this.isWaiting = false;
  }

  viewDetail(id: number) {
    this.router.navigate(['events', id])
  }

  pageUpOrDown(offset: number) {
    this.isWaiting = true;
    this.api.pageUpOrDown(offset, 'events', this.url + this.order).subscribe(data => {
      this.allEvents = data.data.results;
      this.isWaiting = false;
    })
  }

  onSearch(values: any) {
    
    this.isWaiting = true;
    this.url = values.search !== '' ? `nameStartsWith=${values.search}` : '';
    this.order = values.order !== '' ? `&orderBy=${values.order}` : '';

    this.api.setLimit(values.limit)

    this.api.getElementsBySearchField(this.url + this.order, 'events')
      .subscribe(data => { this.initData(data) })    
  }

  ngOnDestroy(): void {
    this.allEvents = [];
  }

}
