import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  limit: number = 20

  constructor(private http: HttpClient) { }

  private getApiUrl(q: string): string {
    let query = q.trim();
    const limit = query.slice(-1) === '?' ? `limit=${this.limit}` : `&limit=${this.limit}`;
    
    // supprimer '=' ou '&', si placer apres le point d'interogation
    const l = query.charAt(query.indexOf("?") + 1);
    if (l === '=' || l === '&') {
      const chars = query.split('');
      chars[query.indexOf(l)] = '';
      query =  chars.join('');
    }

    return environment.apiUrl + query + limit
  }

  handleError(error: HttpErrorResponse) {
    let message = 'Unknow error'
    if (error.error instanceof ErrorEvent) {
      message = "Error Code: " + error.error.message
    } else {
      message = "Error Code: " + error.status + "\n message: " + error.message
    }
    return throwError(message);
  }

  setLimit(limit: number): void {
    this.limit = limit;
  }

  // /v1/public/{element}?
  getAllElements(element: string): Observable<any> {
    return this.http.get<any>(this.getApiUrl(`${element}?`))
      .pipe(catchError(this.handleError))
  }

  // /v1/public/{element}/{id}?
  getElementById(id: number, element: string): Observable<any> {
    return this.http.get<any>(this.getApiUrl(`${element}/${id}?`))
      .pipe(catchError(this.handleError))
  }

  getElementsBySearchField(query: string = '', element: string = ''): Observable<any> {
    let searchUrl = `${element}?${query}`;
    return this.http.get<any>(this.getApiUrl(searchUrl))
      .pipe(catchError(this.handleError))
  }

  // /v1/public/{element}?offset=${offset}&
  pageUpOrDown(offset: number, element: string = '', baseUrl: string = ''): Observable<any> {
    const url = element + "?" + baseUrl
    return this.http.get<any>(this.getApiUrl(`${url}&offset=${offset}&`))
      .pipe(catchError(this.handleError))
  }

  // /v1/public/element/id/colectionName 
  getcollection(url: string): Observable<any> {
    return this.http.get<any>(this.getApiUrl(url + '?')).pipe(catchError(this.handleError))
  }

}