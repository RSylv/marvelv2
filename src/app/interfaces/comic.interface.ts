export interface ComicModel {
  id: number,
  digitalId: number,
  title: string,
  issueNumber: number,
  variantDescription: string,
  description: string,
  modified: string,
  isbn: string,
  upc: string,
  diamondCode: string,
  ean: string,
  issn: string,
  format: string,
  pageCount: 112,
  textObjects: [],
  resourceURI: string,
  variants: object[],
  collections: [],
  collectedIssues: [],
  dates: object[],
  prices: object[],
  thumbnail: object,
  images: [],
  characters: {
    available: number,
    collectionURI: string,
    items: [
      {
        resourceURI: string,
        name: string
      }
    ],
    returned: number
  },
  creators: {
    available: number,
    collectionURI: string,
    items: [
      {
        resourceURI: string,
        name: string
      }
    ],
    returned: number
  },
  series: {
    available: number,
    collectionURI: string,
    items: [
      {
        resourceURI: string,
        name: string
      }
    ],
    returned: number
  },
  stories: {
    available: number,
    collectionURI: string,
    items: [
      {
        resourceURI: string,
        name: string,
        type: string,
      }
    ],
    returned: number
  },
  events: {
    available: number,
    collectionURI: string,
    items: [
      {
        resourceURI: string,
        name: string
      }
    ],
    returned: number
  },
  urls: [
    {
      type: string,
      url: string
    }
  ]
}