import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { HttpsPathService } from 'src/app/services/https-path.service';

@Component({
  selector: 'app-creator',
  templateUrl: './creator.component.html',
  styleUrls: ['./creator.component.scss']
})
export class CreatorComponent implements OnInit {

  private id: number = this.route.snapshot.params.id;
  isWaiting: boolean = true;
  error: string = '';
  creator: any = null;
  listTitle: string[] = ['comics', 'series', 'stories', 'events'];

  constructor(private api: ApiService, private route: ActivatedRoute, private https: HttpsPathService) { }

  ngOnInit(): void {
    this.api.getElementById(this.id, 'creators').subscribe(data => {
      this.creator = data.data.results[0]
      this.creator.thumbnail.path = this.https.httpsUrl(this.creator.thumbnail.path)
      this.isWaiting = false
    })
  }

  checkDate(date: string) {
    if (date.charAt(0) !== '-') {
      return date;
    }
    return '';
  }

}
