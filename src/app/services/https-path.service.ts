import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HttpsPathService {

  /**
   * Transform url http to https
   * @param url 
   * @returns the safe url
   */
  httpsUrl(url: string) {
    return url.slice(0, 5) !== 'https' ? 'https' + url.slice(4, url.length) : url;    
  }

  /**
   * Special fonction for list component
   * @param element (characters, comics, etc..)
   * @returns element 
   */
  httpsUrls(element: Array<any>): Array<object> {
    for (const key in element) {      
      if (element[key].thumbnail !== null) {
        element[key].thumbnail.path = this.httpsUrl(element[key].thumbnail.path)   
      }
    }
    return element;
  }
  
}
