export interface CreatorModel {
    id: number,
    firstName: string,
    middleName: string,
    lastName: string,
    suffix: string,
    fullName: string,
    modified: string,
    thumbnail: object,
    resourceURI: string,
    comics: {
        available: number,
        collectionURI: string,
        items: [
            {
                resourceURI: string,
                name: string
            }
        ],
        returned: number
    },
    series: {
        available: number,
        collectionURI: string,
        items: [
            {
                resourceURI: string,
                name: string
            }
        ],
        returned: number
    },
    stories: {
        available: number,
        collectionURI: string,
        items: [
            {
                resourceURI: string,
                name: string,
                type: string,
            }
        ],
        returned: number
    },
    events: {
        available: number,
        collectionURI: string,
        items: [
            {
                resourceURI: string,
                name: string
            }
        ],
        returned: number
    },
    urls: [
        {
            type: string,
            url: string
        }
    ]
}