import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { HttpsPathService } from 'src/app/services/https-path.service';

@Component({
  selector: 'app-comic',
  templateUrl: './comic.component.html',
  styleUrls: ['./comic.component.scss']
})
export class ComicComponent implements OnInit {

  private id: number = this.route.snapshot.params.id;
  isWaiting: boolean = true;
  error: string = '';
  comic: any = null;
  listTitle: string[] = ['characters', 'creators', 'series', 'stories', 'events'];

  constructor(private api: ApiService, private route: ActivatedRoute, private https: HttpsPathService) { }

  ngOnInit(): void {
    this.api.getElementById(this.id, 'comics').subscribe(data => {
      this.comic = data.data.results[0]
      this.comic.thumbnail.path = this.https.httpsUrl(this.comic.thumbnail.path)
      this.isWaiting = false
    })
  }

  checkDate(date: string) {
    if (date.charAt(0) !== '-') {
      return date;
    }
    return '';
  }

}
