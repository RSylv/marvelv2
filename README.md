# MarvelApi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


<br>  
<hr>  
<br>  
  
## MODEL FRONT  
 
#### Characters :   
```
HEADER :
	- name
	- modified 
BODY :
	LEFT :
        - thumbnail
            - path
            - extension
        - description

	RIGHT:
        4 Listes
        - comics []
            - available
            - returned (Ex. 20/60 results)
            - items
                - resourceURI
                - name
        - series []
            - available
            - returned
            - items
                - resourceURI
                - name
        - stories []
            - available
            - returned
            - items
                - resourceURI
                - name
                - type
        - events []
            - available
            - returned
            - items
                - resourceURI
                - name
FOOTER :
	urls []
	- type 	(afficher dans lien)
	- url	(src du lien)
```
  
  
#### Comics :  
```
id
digitalId
issueNumber
diamondCode

HEADER :
    - title
    - modified
    - isbn
    - upc
    - ean
    - diamondCode
    - format
    - pageCount
    - thumbnail
        - path
        - extension

BODY :
    - description
    - variantDescription
    - images[]
        - path
        - extension
    - characters[]
        - available
        - returned
        - items
            - resourceURI
            - name
    - creators[]
        - available
        - returned
        - items
            - resourceURI
            - name
            - role
    - dates[]
        - type
        - date
    - prices[]
        - type
        - price        
    
    - variants[]
        - resourceURI
        - name
FOOTER :
    - urls[]
        - type
        - url
```

##### Carroussel image Comics
- DOC: https://www.npmjs.com/package/ng-image-slider

INSTALL: 
- npm install ng-image-slider --save
- npm i ts-md5



## Avant de lancer le projet

- npm install


TODO: 
- arrow up (scroll-smooth) ✔ 
- check date function  ✔ 
- events previous and next ✔ 
- animation lors d'appel api  ✔ 
- search fonction !!! (with service ?)  ✔ 
- do the collection component to add Collection Vue  ✔ 
- ajouter choix d'oranisation de listes  ✔ 
- navigation dynamique  ✔ 
- retour si non reponse de l'api ou erreur
- si pas de resultat (search) => write msg !





## Gitlab.ci
- https://dev.to/gaurangdhorda/deploy-angular-project-to-gitlab-pages-using-gitlab-ci-aik

##### [ Step 1 ] : Create .gitlab-ci.yml

Open your angular project. Inside root of your project [ at your package.json file ] create new file called ".gitlab-ci.yml". Inside this file add below script code.


```
image: node:12.19.0
pages:
  cache:
    paths:
      - node_modules/
  script:
    - npm install -g @angular/cli@10.1.3
    - npm install
    - npm run buildProd
  artifacts:
    paths:
      - public
  only:
    - master
    - pages

```  

**First of all,**

1- "image:" is docker image and that image pulls node.js version 12.19.0.
2- Then inside "pages:", we are setting path of node_modules.
3- Inside "script:" first of all we install angular-cli. version is based on your angular project so change it accordingly. 
   All pages projects are served from "public" folder so we have to change path of it too. We only deploy master branch and pages branch, any other branch will not detect inside CI/CD pipeline.


##### [ STEP 2 ] Add new scripts inside "package.json"

```
"buildProd": "ng build --base-href=/your-repo-name/",
```

- here, you need to change base-path and base-path is based on your gitlab repository
To check gitlab repository base path at gitlab.com... settings -> pages and inside this path see Your pages are served under: section like https://(userName).gitlab.io/(projectName)
above /(projectName)/ is your-repo-name.


##### [ STEP 3 ] update output path inside angular.json file

```
"outputPath": "public",
```

After following above steps, if everything is okay then after some time your project is deployed to your pages path url like https://(userName).gitlab.io/(projectName)





@angular/common@^9.1.12
@angular/core@^9.1.12




### Refresh on the same route
- https://medium.com/angular-in-depth/refresh-current-route-in-angular-512a19d58f6e