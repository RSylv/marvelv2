import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharacterComponent } from './components/characters/character/character.component';
import { CharactersComponent } from './components/characters/characters.component';
import { CollectionsComponent } from './components/collections/collections.component';
import { ComicComponent } from './components/comics/comic/comic.component';
import { ComicsComponent } from './components/comics/comics.component';
import { CreatorComponent } from './components/creators/creator/creator.component';
import { CreatorsComponent } from './components/creators/creators.component';
import { EventComponent } from './components/events/event/event.component';
import { EventsComponent } from './components/events/events.component';
import { SerieComponent } from './components/series/serie/serie.component';
import { SeriesComponent } from './components/series/series.component';
import { StorieComponent } from './components/stories/storie/storie.component';
import { StoriesComponent } from './components/stories/stories.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'characters', component: CharactersComponent },
  { path: 'characters/:id', component: CharacterComponent },
  { path: 'comics', component: ComicsComponent },
  { path: 'comics/:id', component: ComicComponent },
  { path: 'series', component: SeriesComponent },
  { path: 'series/:id', component: SerieComponent },
  { path: 'creators', component: CreatorsComponent },
  { path: 'creators/:id', component: CreatorComponent },
  { path: 'stories', component: StoriesComponent },
  { path: 'stories/:id', component: StorieComponent },
  { path: 'events', component: EventsComponent },
  { path: 'events/:id', component: EventComponent },
  { path: 'collection/:name/:id/:title', component: CollectionsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'top', onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
