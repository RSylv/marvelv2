import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-storie',
  templateUrl: './storie.component.html',
  styleUrls: ['./storie.component.scss']
})
export class StorieComponent implements OnInit {

  private id: number = this.route.snapshot.params.id;
  isWaiting: boolean = true;
  error: string = '';
  storie: any = null;
  listTitle: string[] = ['creators', 'characters', 'series', 'comics', 'events'];

  constructor(private api: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.api.getElementById(this.id, 'stories').subscribe(data => {
      this.storie = data.data.results[0]
      this.isWaiting = false   
    })
  }

  goToComic(url: string): void {    
    this.id = Number(url.slice(url.lastIndexOf('/') + 1, url.length))
    this.router.navigate(['comics', this.id])
  }

  checkDate(date: string) {
    if (date.charAt(0) !== '-') {
      return date;
    }
    return '';
  }

}
