export interface TableModel {
    available?: number;
    collectionURI?: string;
    items?: [
        {
            resourceURI?: string;
            name?: string;
            type?: string;
        }
    ]
    returned?: number;
}