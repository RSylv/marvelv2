import { TestBed } from '@angular/core/testing';

import { HttpsPathService } from './https-path.service';

describe('HttpsPathService', () => {
  let service: HttpsPathService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpsPathService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
