export const environment = {
  production: true,
  publicKey: 'publicKey',
  privateKey: 'privateKey',
  apiUrl: 'https://gateway.marvel.com/v1/public/'
};
