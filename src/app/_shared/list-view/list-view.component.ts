import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HttpsPathService } from 'src/app/services/https-path.service';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent implements OnInit {

  @Input() allElements: any[] = [];
  @Output() viewDetails: EventEmitter<any> = new EventEmitter();

  constructor(private https: HttpsPathService) { }

  ngOnInit(): void {
    this.allElements = this.https.httpsUrls(this.allElements)
  }

  viewDetail(id: number) {
    this.viewDetails.emit(id)
  }

}
