import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CharacterModel } from 'src/app/interfaces/character.interface';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {

  private url: string = '';
  private order: string = '';
  isWaiting: boolean = true;
  name: string = 'characters';
  allCharacters: CharacterModel[] = [];
  total: number = 0;
  offset: number = 0;
  limit: number = 0;
  page: number = 1;

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.api.getAllElements('characters').subscribe(data => {
      this.initData(data)
    })
  }

  initData(data: any): void {
    this.allCharacters = data.data.results;
    this.total = data.data.total;
    this.offset = data.data.offset;
    this.limit = data.data.limit;
    this.isWaiting = false;
  }

  viewDetail(id: number) {
    this.router.navigate(['characters', id])
  }

  pageUpOrDown(offset: number) {
    this.isWaiting = true;
    this.api.pageUpOrDown(offset, 'characters', this.url + this.order).subscribe(data => {
      this.allCharacters = data.data.results;
      this.isWaiting = false;
    })
  }

  onSearch(values: any) {

    this.isWaiting = true;
    this.url = values.search !== '' ? `nameStartsWith=${values.search}` : '';
    this.order = values.order !== '' ? `&orderBy=${values.order}` : '';

    this.api.setLimit(values.limit)

    this.api.getElementsBySearchField(this.url + this.order, 'characters')
      .subscribe(data => { this.initData(data) })
    
  }

  ngOnDestroy(): void {
    this.allCharacters = [];
  }

}
