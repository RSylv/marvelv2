import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OrderList } from 'src/app/interfaces/order-list.interface';
import { OrderSelect } from 'src/app/interfaces/order-select.interface';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  @Input() componentName: string = '';
  @Output() onSearchClick = new EventEmitter();

  limits: string[] = ["10", "20", "25", "50", "75", "100"];
  types: string[] = [
    "collection",
    "one shot",
    "limited",
    "ongoing"
  ];
  formats: string[] = [
    "Comic",
    "Digital comic",
    "Graphic novel",
    "Magazine",
    "Digest",
    "Trade Paperback",
    "hardcover",
    "Infinite comic"
  ];
  orderCharacters: OrderSelect[] = this.makeSelectionList([
    { name: "name", order: false },
    { name: "modified", order: true }
  ])
  orderComics: OrderSelect[] = this.makeSelectionList([
    { name: "focDate", order: true },
    { name: "onsaleDate", order: true },
    { name: "title", order: false },
    { name: "issueNumber", order: true },
    { name: "modified", order: true }
  ])
  orderCreator: OrderSelect[] = this.makeSelectionList([
    { name: "lastName", order: false },
    { name: "firstName", order: false },
    { name: "MiddleName", order: false },
    // { name: "suffix", order: true },
    { name: "modified", order: true }
  ])
  orderEvents: OrderSelect[] = this.makeSelectionList([
    { name: "title", order: false },
    { name: "startDate", order: true },
    { name: "modified", order: true }
  ])
  orderSeries: OrderSelect[] = this.makeSelectionList([
    { name: "title", order: false },
    { name: "startYear", order: true },
    { name: "modified", order: true }
  ])
  orderStories: OrderSelect[] = this.makeSelectionList([
    { name: "id", order: true },
    { name: "modified", order: true }
  ])

  searchForm = new FormGroup({
    search: new FormControl(''),
    firstname: new FormControl('', [Validators.minLength(0), Validators.maxLength(50)]),
    lastname: new FormControl('', [Validators.minLength(0), Validators.maxLength(50)]),
    limit: new FormControl('20', [Validators.min(0), Validators.max(100)]),
    format: new FormControl(''),
    type: new FormControl(''),
    order: new FormControl(''),
  });

  constructor(private api: ApiService) { }

  ngOnInit(): void {
  }

  onSearch() {
    this.onSearchClick.emit(this.searchForm.value)
  }

  makeSelectionList(list: OrderList[]): OrderSelect[] {
    const selectionList: OrderSelect[] = [];
    list.forEach(li => {
      let order = !li.order ? '[A-Z]' : '[+/-]';
      let up = { text: li.name, value: li.name, order }
      selectionList.push(up)
      order = !li.order ? '[Z-A]' : '[-/+]';
      let down = { text: li.name, value: '-' + li.name, order }
      selectionList.push(down)
    })
    return selectionList;
  }

}
